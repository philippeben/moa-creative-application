<?php
$file = file_get_contents('./films.json', FILE_USE_INCLUDE_PATH);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Step 3</title>
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<link href="jsontree.css" rel="stylesheet">
	<script src="jsontree.js"></script>
</head>
    <body>
        <div id="treeview"></div>
    </body>
    <script>
    var json = [<?php echo $file; ?>];
    document.getElementById("treeview").innerHTML = JSONTree.create(json);
    </script>
</html>

